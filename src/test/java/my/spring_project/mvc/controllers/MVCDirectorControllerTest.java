package my.spring_project.mvc.controllers;

import lombok.extern.slf4j.Slf4j;
import my.spring_project.dto.DirectorDTO;
import my.spring_project.dto.DirectorSearchDTO;
import my.spring_project.mapper.DirectorMapper;
import my.spring_project.service.DirectorService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Slf4j
public class MVCDirectorControllerTest extends  CommonTestMVC {

    @Autowired
    private DirectorService directorService;
    @Autowired
    private DirectorMapper directorMapper;

    private final DirectorDTO directorDTO = new DirectorDTO("MVCFio1",
            "MVCPosition1", "", new ArrayList<>());
    private final DirectorDTO updateDirectorDTO = new DirectorDTO("MVCFio1Updated",
            "MVCPosition1Updated", "", new ArrayList<>());

    @Override
    @Test
    @DisplayName("Test listAll() MVC")
    @Order(0)
    @WithAnonymousUser
    protected void listAll() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/directors")
                        .param("page","1")
                        .param("size","5")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("directors/allDirectors"))
                .andExpect(model().attributeExists("directors"))
                .andReturn();
    }

    @Override
    @Test
    @DisplayName("Test create() MVC")
    @Order(1)
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    protected void createObject() throws Exception {
        File file = new File("src/main/resources/static/img/No-Image.png");
        MockMultipartFile upload = new MockMultipartFile("file", "file.png",
                MediaType.IMAGE_PNG_VALUE,
                Files.readAllBytes(file.toPath()));
        mvc.perform(MockMvcRequestBuilders.multipart("/directors/add-director")
                        .file(upload)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .flashAttr("directorForm", directorDTO)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/directors"))
                .andExpect(redirectedUrlTemplate("/directors"))
                .andExpect(redirectedUrl("/directors"));
    }

    @Override
    @Test
    @DisplayName("Test update() MVC")
    @Order(2)
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    protected void updateObject() throws Exception {
        File file = new File("src/main/resources/static/img/No-Image.png");
        MockMultipartFile upload = new MockMultipartFile("file", "file.png",
                MediaType.IMAGE_PNG_VALUE,
                Files.readAllBytes(file.toPath()));
        PageRequest pageRequest = PageRequest.of(0, 5, Sort.by(Sort.Direction.ASC, "directorFio"));
        DirectorSearchDTO directorSearchDTO = new DirectorSearchDTO();
        directorSearchDTO.setDirectorFio(directorDTO.getDirectorFio());
        directorSearchDTO.setPosition(directorDTO.getPosition());
        DirectorDTO directorDTOForUpdate = directorMapper.toDTO(directorService
                .findDirectors(directorSearchDTO, pageRequest)
                .getContent().get(0));
        directorDTOForUpdate.setDirectorFio(updateDirectorDTO.getDirectorFio());
        directorDTOForUpdate.setPosition(updateDirectorDTO.getPosition());
        mvc.perform(multipart("/directors/update")
                        .file(upload)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .flashAttr("directorForm", directorDTOForUpdate)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/directors"))
                .andExpect(redirectedUrl("/directors"));
    }

    @Override
    @Test
    @DisplayName("Test delete() MVC")
    @Order(3)
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    protected void deleteObject() throws Exception {
        PageRequest pageRequest = PageRequest.of(0, 5, Sort.by(Sort.Direction.ASC, "directorFio"));
        DirectorSearchDTO directorSearchDTO = new DirectorSearchDTO();
        directorSearchDTO.setDirectorFio(updateDirectorDTO.getDirectorFio());
        directorSearchDTO.setPosition(updateDirectorDTO.getPosition());
        Long directorDeleteId = directorService
                .findDirectors(directorSearchDTO, pageRequest)
                .getContent().get(0).getId();
        mvc.perform(get("/directors/delete/{id}", directorDeleteId)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/directors"))
                .andExpect(redirectedUrl("/directors"));
        DirectorDTO deletedDirector = directorService.getOne(directorDeleteId);
        assertTrue(deletedDirector.getIsDeleted());
    }

    @Test
    @DisplayName("Test restore() MVC")
    @Order(4)
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    protected void restoreObject() throws Exception {
        PageRequest pageRequest = PageRequest.of(0, 5, Sort.by(Sort.Direction.ASC, "directorFio"));
        DirectorSearchDTO directorSearchDTO = new DirectorSearchDTO();
        directorSearchDTO.setDirectorFio(updateDirectorDTO.getDirectorFio());
        directorSearchDTO.setPosition(updateDirectorDTO.getPosition());
        Long directorRestoreId = directorService
                .findDirectors(directorSearchDTO, pageRequest)
                .getContent().get(0).getId();
        mvc.perform(get("/directors/recovery/{id}", directorRestoreId)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/directors"))
                .andExpect(redirectedUrl("/directors"));
        DirectorDTO restoredDirector = directorService.getOne(directorRestoreId);
        assertFalse(restoredDirector.getIsDeleted());
        directorService.delete(restoredDirector.getId());
    }
}
