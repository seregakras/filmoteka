package my.spring_project.mvc.controllers;

import lombok.extern.slf4j.Slf4j;
import my.spring_project.dto.FilmDTO;
import my.spring_project.dto.FilmWithDirectorsNameAndRatingDTO;
import my.spring_project.model.Genre;
import my.spring_project.service.FilmService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.File;
import java.nio.file.Files;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Slf4j
public class MVCFilmControllerTest extends  CommonTestMVC {

    @Autowired
    private FilmService filmService;

    private final FilmDTO filmDTO = new FilmDTO("titleTest",
            2001, "countryTest", Genre.ACTION, "", null, null, null);
    private final FilmDTO updateFilmDTO = new FilmDTO("titleTestUpdated",
            2002, "countryTestUpdated", Genre.ACTION, "", null, null, null);

    @Override
    @Test
    @DisplayName("Test listAll() MVC")
    @Order(0)
    @WithAnonymousUser
    protected void listAll() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/films")
                        .param("page","1")
                        .param("size","5")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("films/allFilms"))
                .andExpect(model().attributeExists("films"))
                .andReturn();
    }

    @Override
    @Test
    @DisplayName("Test create() MVC")
    @Order(1)
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    protected void createObject() throws Exception {
        File file = new File("src/main/resources/static/img/No-Image.png");
        MockMultipartFile upload = new MockMultipartFile("file", "file.png",
                MediaType.IMAGE_PNG_VALUE,
                Files.readAllBytes(file.toPath()));
        mvc.perform(MockMvcRequestBuilders.multipart("/films/add-film")
                        .file(upload)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .flashAttr("filmForm", filmDTO)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/films"))
                .andExpect(redirectedUrlTemplate("/films"))
                .andExpect(redirectedUrl("/films"));
    }

    @Override
    @Test
    @DisplayName("Test update() MVC")
    @Order(2)
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    protected void updateObject() throws Exception {
        File file = new File("src/main/resources/static/img/No-Image.png");
        MockMultipartFile upload = new MockMultipartFile("file", "file.png",
                MediaType.IMAGE_PNG_VALUE,
                Files.readAllBytes(file.toPath()));
        PageRequest pageRequest = PageRequest.of(0, 5, Sort.by(Sort.Direction.ASC, "title"));
        FilmWithDirectorsNameAndRatingDTO filmSearchDTO = new FilmWithDirectorsNameAndRatingDTO();
        filmSearchDTO.setTitle(filmDTO.getTitle());
        filmSearchDTO.setGenre(filmDTO.getGenre());
        filmSearchDTO.setCountry(filmDTO.getCountry());
        FilmWithDirectorsNameAndRatingDTO filmDTOForUpdate = filmService
                .searchFilms(filmSearchDTO, pageRequest)
                .getContent().get(0);
        filmDTOForUpdate.setTitle(updateFilmDTO.getTitle());
        filmDTOForUpdate.setCountry(updateFilmDTO.getCountry());
        mvc.perform(multipart("/films/update")
                        .file(upload)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .flashAttr("filmForm", filmDTOForUpdate)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/films"))
                .andExpect(redirectedUrl("/films"));
    }

    @Override
    @Test
    @DisplayName("Test delete() MVC")
    @Order(3)
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    protected void deleteObject() throws Exception {
        PageRequest pageRequest = PageRequest.of(0, 5, Sort.by(Sort.Direction.ASC, "title"));
        FilmWithDirectorsNameAndRatingDTO filmSearchDTO = new FilmWithDirectorsNameAndRatingDTO();
        filmSearchDTO.setTitle(updateFilmDTO.getTitle());
        filmSearchDTO.setGenre(updateFilmDTO.getGenre());
        filmSearchDTO.setCountry(updateFilmDTO.getCountry());
        Long filmDeleteId = filmService
                .searchFilms(filmSearchDTO, pageRequest)
                .getContent().get(0).getId();
        mvc.perform(get("/films/delete/{id}", filmDeleteId)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/films"))
                .andExpect(redirectedUrl("/films"));
        FilmDTO deletedFilm = filmService.getOne(filmDeleteId);
        assertTrue(deletedFilm.getIsDeleted());
    }

    @Test
    @DisplayName("Test restore() MVC")
    @Order(4)
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    protected void restoreObject() throws Exception {
        PageRequest pageRequest = PageRequest.of(0, 5, Sort.by(Sort.Direction.ASC, "title"));
        FilmWithDirectorsNameAndRatingDTO filmSearchDTO = new FilmWithDirectorsNameAndRatingDTO();
        filmSearchDTO.setTitle(updateFilmDTO.getTitle());
        filmSearchDTO.setGenre(updateFilmDTO.getGenre());
        filmSearchDTO.setCountry(updateFilmDTO.getCountry());
        Long filmRestoreId = filmService
                .searchFilms(filmSearchDTO, pageRequest)
                .getContent().get(0).getId();
        mvc.perform(get("/films/recovery/{id}", filmRestoreId)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/films"))
                .andExpect(redirectedUrl("/films"));
        FilmDTO restoredFilm = filmService.getOne(filmRestoreId);
        assertFalse(restoredFilm.getIsDeleted());
        filmService.delete(restoredFilm.getId());
    }
}
