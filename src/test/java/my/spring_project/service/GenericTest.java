package my.spring_project.service;

import my.spring_project.dto.GenericDTO;
import my.spring_project.mapper.GenericMapper;
import my.spring_project.model.GenericModel;
import my.spring_project.repositories.GenericRepository;

public abstract class GenericTest<E extends GenericModel, D extends GenericDTO>{

    protected GenericService<E, D> service;
    protected GenericRepository<E> repository;
    protected GenericMapper<E, D> mapper;

//    @BeforeEach
//    void  init() {
//        UsernamePasswordAuthenticationToken authentication =
//                new UsernamePasswordAuthenticationToken(CustomUserDetails.builder()
//                        .username("USER"),
//                        null,
//                        null);
//        SecurityContextHolder.getContext().setAuthentication(authentication);
//    }

    protected abstract void getAll();
    protected abstract void getOne();
    protected abstract void create();
    protected abstract void update();
    protected abstract void delete();

}
