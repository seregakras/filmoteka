package my.spring_project.service;

import my.spring_project.dto.FilmDTO;
import my.spring_project.model.Film;
import my.spring_project.model.Genre;

import java.util.Arrays;
import java.util.List;

public interface FilmTestData {

    FilmDTO FILM_DTO1 = new FilmDTO("film1",
            2001,
            "country1",
            Genre.ACTION,
            "",
            null,
            null,
            null);

    FilmDTO FILM_DTO2 = new FilmDTO("film2",
            2002,
            "country2",
            Genre.ACTION,
            "",
            null,
            null,
            null);

    FilmDTO FILM_DTO3 = new FilmDTO("film3",
            2003,
            "country3",
            Genre.ACTION,
            "",
            null,
            null,
            null);

    List<FilmDTO> FILM_DTO_LIST = Arrays.asList(FILM_DTO1, FILM_DTO2, FILM_DTO3);


    Film FILM1 = new Film("film1",
            2001,
            "country1",
            Genre.ACTION,
            "",
            null,
            null,
            null);

    Film FILM2 = new Film("film2",
            2002,
            "country2",
            Genre.ACTION,
            "",
            null,
            null,
            null);

    Film FILM3 = new Film("film3",
            2003,
            "country3",
            Genre.ACTION,
            "",
            null,
            null,
            null);

    List<Film> FILM_LIST = Arrays.asList(FILM1, FILM2, FILM3);
}

