package my.spring_project.service;

import lombok.extern.slf4j.Slf4j;
import my.spring_project.dto.DirectorDTO;
import my.spring_project.mapper.DirectorMapper;
import my.spring_project.model.Director;
import my.spring_project.repositories.DirectorRepository;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class DirectorServiceTest extends GenericTest<Director, DirectorDTO> {


    public DirectorServiceTest() {
        super();
        repository = Mockito.mock(DirectorRepository.class);
        mapper = Mockito.mock(DirectorMapper.class);
        service = new DirectorService((DirectorRepository) repository, (DirectorMapper) mapper);
    }

    @Test
    @Order(1)
    @Override
    protected void getAll() {
        Mockito.when(repository.findAll()).thenReturn(DirectorTestData.DIRECTOR_LIST);
        Mockito.when(mapper.toDTOs(DirectorTestData.DIRECTOR_LIST))
                .thenReturn(DirectorTestData.DIRECTOR_DTO_LIST);
        List<DirectorDTO> directorDTOList = service.listAll();
        log.info("Testing getAll(): " + directorDTOList);
        assertEquals(DirectorTestData.DIRECTOR_LIST.size(), directorDTOList.size());
    }

    @Test
    @Order(2)
    @Override
    protected void getOne() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(DirectorTestData.DIRECTOR_1));
        Mockito.when(mapper.toDTO(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_DTO_1);
        DirectorDTO directorDTO = service.getOne(1L);
        log.info("Testing getOne(): " + directorDTO);
        assertEquals(DirectorTestData.DIRECTOR_DTO_1, directorDTO);
    }

    @Test
    @Override
    @Order(3)
    protected void create() {
        Mockito.when(mapper.toEntity(DirectorTestData.DIRECTOR_DTO_1)).thenReturn(DirectorTestData.DIRECTOR_1);
        Mockito.when(mapper.toDTO(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_DTO_1);
        Mockito.when(repository.save(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_1);
        DirectorDTO directorDTO = service.create(DirectorTestData.DIRECTOR_DTO_1);
        log.info("Testing create(): " + directorDTO);
        assertEquals(DirectorTestData.DIRECTOR_DTO_1, directorDTO);
    }

    @Test
    @Order(4)
    @Override
    protected void update() {
        Mockito.when(mapper.toEntity(DirectorTestData.DIRECTOR_DTO_1)).thenReturn(DirectorTestData.DIRECTOR_1);
        Mockito.when(mapper.toDTO(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_DTO_1);
        Mockito.when(repository.save(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_1);
        DirectorDTO directorDTO = service.update(DirectorTestData.DIRECTOR_DTO_1);
        log.info("Testing update(): " + directorDTO);
        assertEquals(DirectorTestData.DIRECTOR_DTO_1, directorDTO);
    }

    @Override
    protected void delete() {

    }


}
