package my.spring_project.service;

import lombok.extern.slf4j.Slf4j;
import my.spring_project.dto.FilmDTO;
import my.spring_project.mapper.FilmMapper;
import my.spring_project.mapper.FilmWithDirectorsMapper;
import my.spring_project.model.Film;
import my.spring_project.repositories.FilmRepository;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class FilmServiceTest extends GenericTest<Film, FilmDTO> {


    public FilmServiceTest() {
        super();
        repository = Mockito.mock(FilmRepository.class);
        mapper = Mockito.mock(FilmMapper.class);
        FilmWithDirectorsMapper mapper1 = Mockito.mock(FilmWithDirectorsMapper.class);
        service = new FilmService((FilmRepository) repository,
                (FilmMapper) mapper, mapper1);
    }

    @Test
    @Order(1)
    @Override
    protected void getAll() {
        Mockito.when(repository.findAll()).thenReturn(FilmTestData.FILM_LIST);
        Mockito.when(mapper.toDTOs(FilmTestData.FILM_LIST))
                .thenReturn(FilmTestData.FILM_DTO_LIST);
        List<FilmDTO> filmDTOList = service.listAll();
        log.info("Testing getAll(): " + filmDTOList);
        assertEquals(FilmTestData.FILM_LIST.size(), filmDTOList.size());
    }

    @Test
    @Order(2)
    @Override
    protected void getOne() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(FilmTestData.FILM1));
        Mockito.when(mapper.toDTO(FilmTestData.FILM1)).thenReturn(FilmTestData.FILM_DTO1);
        FilmDTO filmDTO = service.getOne(1L);
        log.info("Testing getOne(): " + filmDTO);
        assertEquals(FilmTestData.FILM_DTO1, filmDTO);
    }

    @Test
    @Override
    @Order(3)
    protected void create() {
        Mockito.when(mapper.toEntity(FilmTestData.FILM_DTO1)).thenReturn(FilmTestData.FILM1);
        Mockito.when(mapper.toDTO(FilmTestData.FILM1)).thenReturn(FilmTestData.FILM_DTO1);
        Mockito.when(repository.save(FilmTestData.FILM1)).thenReturn(FilmTestData.FILM1);
        FilmDTO filmDTO = service.create(FilmTestData.FILM_DTO1);
        log.info("Testing create(): " + filmDTO);
        assertEquals(FilmTestData.FILM_DTO1, filmDTO);
    }

    @Test
    @Order(4)
    @Override
    protected void update() {
        Mockito.when(mapper.toEntity(FilmTestData.FILM_DTO1)).thenReturn(FilmTestData.FILM1);
        Mockito.when(mapper.toDTO(FilmTestData.FILM1)).thenReturn(FilmTestData.FILM_DTO1);
        Mockito.when(repository.save(FilmTestData.FILM1)).thenReturn(FilmTestData.FILM1);
        FilmDTO filmDTO = service.update(FilmTestData.FILM_DTO1);
        log.info("Testing create(): " + filmDTO);
        assertEquals(FilmTestData.FILM_DTO1, filmDTO);
    }

    @Override
    protected void delete() {

    }


}
