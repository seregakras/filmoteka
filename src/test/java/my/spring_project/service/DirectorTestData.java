package my.spring_project.service;

import my.spring_project.dto.DirectorDTO;
import my.spring_project.model.Director;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface DirectorTestData {

    DirectorDTO DIRECTOR_DTO_1 = new DirectorDTO("directorFio1",
            "position1",
            "",
            new ArrayList<>());

    DirectorDTO DIRECTOR_DTO_2 = new DirectorDTO("directorFio2",
            "position2",
            "",
            new ArrayList<>());

    DirectorDTO DIRECTOR_DTO_3 = new DirectorDTO("directorFio3",
            "position3",
            "",
            new ArrayList<>());

    List<DirectorDTO> DIRECTOR_DTO_LIST = Arrays.asList(DIRECTOR_DTO_1, DIRECTOR_DTO_2, DIRECTOR_DTO_3);


    Director DIRECTOR_1 = new Director("directorFio1",
            "position1",
            "",
            null);

    Director DIRECTOR_2 = new Director("directorFio2",
            "position2",
            "",
            null);

    Director DIRECTOR_3 = new Director("directorFio3",
            "position3",
            "",
            null);

    List<Director> DIRECTOR_LIST = Arrays.asList(DIRECTOR_1, DIRECTOR_2, DIRECTOR_3);
}

