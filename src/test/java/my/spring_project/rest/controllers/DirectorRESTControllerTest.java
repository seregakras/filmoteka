package my.spring_project.rest.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import my.spring_project.dto.DirectorDTO;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@Slf4j
class DirectorRESTControllerTest extends CommonTestREST{

    private static Long createdDirectorTestId;

    @Test
    @Order(0)
    @Override
    protected void listAll() throws Exception {
        log.info("Test listAll() is started");
        String result = mvc.perform(MockMvcRequestBuilders.get("/rest/directors/getAll")
                .headers(super.headers)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                ).andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.*", hasSize(greaterThan(0))))
                .andReturn()
                .getResponse()
                .getContentAsString();
        List<DirectorDTO> directorDTOS = objectMapper.readValue(result, new TypeReference<>() {
        });
        directorDTOS.forEach(m -> log.info(m.toString()));
        log.info("Test listAll() is finished");
    }

    @Test
    @Order(1)
    @Override
    protected void createObject() throws Exception {
        log.info("Test add() is started");
        DirectorDTO director = new DirectorDTO("fioTest", "positionTest", "", new ArrayList<>());
        DirectorDTO createdDirector = objectMapper.readValue(mvc.perform(MockMvcRequestBuilders.post("/rest/directors/add")
                        .headers(super.headers)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(director))
                        .accept(MediaType.APPLICATION_JSON)
                ).andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andReturn()
                .getResponse()
                .getContentAsString(),
                DirectorDTO.class);
        createdDirectorTestId = createdDirector.getId();
        log.info(director.toString());
        log.info("Id: {}", createdDirectorTestId);
        log.info("Test add() is finished");
    }

    @Test
    @Order(2)
    @Override
    protected void updateObject() throws Exception {
        log.info("Test update() is started");
        log.info("Id: {}", createdDirectorTestId);
        DirectorDTO existingDirector = objectMapper.readValue(mvc.perform(MockMvcRequestBuilders.get("/rest/directors/getOneById")
                                .headers(super.headers)
                                .contentType(MediaType.APPLICATION_JSON)
                                .param("id", String.valueOf(createdDirectorTestId))
                                .accept(MediaType.APPLICATION_JSON)
                        ).andDo(print())
                        .andExpect(status().is2xxSuccessful())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(),
                DirectorDTO.class);
        existingDirector.setDirectorFio("fioTest_UPDATE");
        existingDirector.setPosition("positionTest_UPDATE");
        mvc.perform(MockMvcRequestBuilders.put("/rest/directors/update")
                        .headers(super.headers)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(existingDirector))
                        .param("id", String.valueOf(createdDirectorTestId))
                        .accept(MediaType.APPLICATION_JSON)
                ).andDo(print())
                .andExpect(status().is2xxSuccessful());
        log.info("Test update() is finished");
    }

    @Test
    @Order(3)
    void addFilm() throws Exception {
        log.info("Test addFilm() is started");
        DirectorDTO director1 = objectMapper.readValue(mvc.perform(MockMvcRequestBuilders.get("/rest/directors/getOneById")
                                .headers(super.headers)
                                .contentType(MediaType.APPLICATION_JSON)
                                .param("id", String.valueOf(1L))
                                .accept(MediaType.APPLICATION_JSON)
                        )
                        .andReturn()
                        .getResponse()
                        .getContentAsString(),
                DirectorDTO.class);
        Long filmAddId = director1.getFilmIds().get(0);
        String result = mvc.perform(MockMvcRequestBuilders.post("/rest/directors/addFilm")
                                .headers(super.headers)
                                .contentType(MediaType.APPLICATION_JSON)
                                .param("directorId", String.valueOf(createdDirectorTestId))
                                .param("filmId", String.valueOf(filmAddId))
                                .accept(MediaType.APPLICATION_JSON)
                        ).andDo(print())
                        .andExpect(status().is2xxSuccessful())
                        .andReturn()
                        .getResponse()
                        .getContentAsString();
        DirectorDTO directorDTO = objectMapper.readValue(result, DirectorDTO.class);
        log.info("Test addFilm() is finished. Result: {}", directorDTO);
    }

    @Test
    @Order(4)
    @Override
    protected void deleteObject() throws Exception {
        log.info("Test delete() is started");
        mvc.perform(MockMvcRequestBuilders.delete("/rest/directors/delete")
                                .headers(super.headers)
                                .contentType(MediaType.APPLICATION_JSON)
                                .param("id", String.valueOf(createdDirectorTestId))
                                .accept(MediaType.APPLICATION_JSON)
                        ).andDo(print())
                        .andExpect(status().is2xxSuccessful());
        DirectorDTO existingDirector = objectMapper.readValue(mvc.perform(MockMvcRequestBuilders.get("/rest/directors/getOneById")
                                .headers(super.headers)
                                .contentType(MediaType.APPLICATION_JSON)
                                .param("id", String.valueOf(createdDirectorTestId))
                                .accept(MediaType.APPLICATION_JSON)
                        ).andDo(print())
                        .andExpect(status().is2xxSuccessful())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(),
                DirectorDTO.class);
        org.junit.jupiter.api.Assertions.assertTrue(existingDirector.getIsDeleted());
        log.info("Test delete() is finished");
        mvc.perform(MockMvcRequestBuilders.delete("/rest/directors/delete/hard")
                        .headers(super.headers)
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("id", String.valueOf(createdDirectorTestId))
                        .accept(MediaType.APPLICATION_JSON)
                ).andDo(print())
                .andExpect(status().is2xxSuccessful());
        log.info("Data is clear!");
    }

}
