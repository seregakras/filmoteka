package my.spring_project.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import static my.spring_project.constants.ImageDirectory.IMAGE_UPLOAD_DIRECTORY;

@Slf4j
public class FileHelper {

    public FileHelper() {
    }

    public static String createFile(final MultipartFile file, String name) {
        String resultFileName = "";
        try {
            Path path = Paths.get(IMAGE_UPLOAD_DIRECTORY + "/" + name + ".jpg");
            if(!path.toFile().exists()) {
                Files.createDirectories(path);
            }
            Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
            resultFileName = path.toString();
        } catch (IOException e) {
            log.error("FileHelper#createFile {}", e.getMessage());
        }
        return resultFileName;
    }
}
