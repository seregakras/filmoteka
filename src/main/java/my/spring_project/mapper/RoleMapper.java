package my.spring_project.mapper;

import jakarta.annotation.PostConstruct;
import my.spring_project.dto.RoleDTO;
import my.spring_project.model.GenericModel;
import my.spring_project.model.Role;
import my.spring_project.repositories.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class RoleMapper extends GenericMapper<Role, RoleDTO> {

    private final UserRepository userRepository;

    protected RoleMapper(ModelMapper modelMapper, UserRepository userRepository) {
        super(Role.class, RoleDTO.class, modelMapper);
        this.userRepository = userRepository;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Role.class, RoleDTO.class)
                .addMappings(m -> m.skip(RoleDTO::setUserIds))
                .setPostConverter(toDTOConverter());

        modelMapper.createTypeMap(RoleDTO.class, Role.class)
                .addMappings(m -> m.skip(Role::setUsers))
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(RoleDTO source, Role destination) {
        if(!Objects.isNull(source.getUserIds())) {
            destination.setUsers(userRepository.findAllById(source.getUserIds()));
        }
        else {
            destination.setUsers(Collections.emptyList());
        }
    }

    @Override
    protected void mapSpecificFields(Role source, RoleDTO destination) {
        destination.setUserIds(getIds(source));
    }

    private List<Long> getIds(Role source) {
        return Objects.isNull(source) || Objects.isNull(source.getUsers())?
                Collections.emptyList() :
                source.getUsers()
                        .stream()
                        .map(GenericModel::getId)
                        .collect(Collectors.toList());
    }
}
