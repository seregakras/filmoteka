package my.spring_project.mapper;

import jakarta.annotation.PostConstruct;
import my.spring_project.dto.ReviewWithFilmDTO;
import my.spring_project.model.Review;
import my.spring_project.repositories.FilmRepository;
import my.spring_project.repositories.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ReviewWithFilmMapper extends GenericMapper<Review, ReviewWithFilmDTO> {

    private final UserRepository userRepository;
    private final FilmRepository filmRepository;
    private final FilmMapper filmMapper;

    protected ReviewWithFilmMapper(ModelMapper modelMapper, UserRepository userRepository,
                                   FilmRepository filmRepository, FilmMapper filmMapper) {
        super(Review.class, ReviewWithFilmDTO.class, modelMapper);
        this.userRepository = userRepository;
        this.filmRepository = filmRepository;
        this.filmMapper = filmMapper;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Review.class, ReviewWithFilmDTO.class)
                .addMappings(m -> m.skip(ReviewWithFilmDTO::setUserId))
                .addMappings(m -> m.skip(ReviewWithFilmDTO::setFilmId))
                .setPostConverter(toDTOConverter());

        modelMapper.createTypeMap(ReviewWithFilmDTO.class, Review.class)
                .addMappings(m -> m.skip(Review::setUser))
                .addMappings(m -> m.skip(Review::setFilm))
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(ReviewWithFilmDTO source, Review destination) {
        destination.setFilm(filmRepository.findById(source.getFilmId()).orElseThrow());
        destination.setUser(userRepository.findById(source.getUserId()).orElseThrow());
    }

    @Override
    protected void mapSpecificFields(Review source, ReviewWithFilmDTO destination) {
        destination.setFilmId(source.getFilm().getId());
        destination.setUserId(source.getUser().getId());
        destination.setReviewFilm(filmMapper.toDTO(source.getFilm()));
    }
}
