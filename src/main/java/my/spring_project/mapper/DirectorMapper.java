package my.spring_project.mapper;

import jakarta.annotation.PostConstruct;
import my.spring_project.dto.DirectorDTO;
import my.spring_project.model.Director;
import my.spring_project.model.GenericModel;
import my.spring_project.repositories.FilmRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class DirectorMapper extends GenericMapper<Director, DirectorDTO>  {

    private final FilmRepository filmRepository;

    protected DirectorMapper(ModelMapper modelMapper, FilmRepository filmRepository) {
        super(Director.class, DirectorDTO.class, modelMapper);
        this.filmRepository = filmRepository;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Director.class, DirectorDTO.class)
                .addMappings(m -> m.skip(DirectorDTO::setFilmIds))
                .setPostConverter(toDTOConverter());

        modelMapper.createTypeMap(DirectorDTO.class, Director.class)
                .addMappings(m -> m.skip(Director::setFilms))
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(DirectorDTO source, Director destination) {
        if(!Objects.isNull(source.getFilmIds())) {
            destination.setFilms(filmRepository.findAllById(source.getFilmIds()));
        }
        else {
            destination.setFilms(Collections.emptyList());
        }
    }

    @Override
    protected void mapSpecificFields(Director source, DirectorDTO destination) {
        destination.setFilmIds(getIds(source));
    }

    private List<Long> getIds(Director source) {
        return Objects.isNull(source) || Objects.isNull(source.getFilms())?
                Collections.emptyList() :
                source.getFilms()
                        .stream()
                        .map(GenericModel::getId)
                        .collect(Collectors.toList());
    }
}
