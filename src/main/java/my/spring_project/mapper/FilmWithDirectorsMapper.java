package my.spring_project.mapper;

import jakarta.annotation.PostConstruct;
import my.spring_project.dto.FilmWithDirectorsNameAndRatingDTO;
import my.spring_project.dto.ReviewDTO;
import my.spring_project.model.Film;
import my.spring_project.model.GenericModel;
import my.spring_project.repositories.DirectorRepository;
import my.spring_project.repositories.OrderRepository;
import my.spring_project.repositories.ReviewRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class FilmWithDirectorsMapper extends GenericMapper<Film, FilmWithDirectorsNameAndRatingDTO> {

    private final DirectorRepository directorRepository;
    private final OrderRepository orderRepository;
    private final ReviewRepository reviewRepository;
    private final ReviewMapper reviewMapper;

    protected FilmWithDirectorsMapper(ModelMapper modelMapper, DirectorRepository directorRepository,
                                      OrderRepository orderRepository, ReviewRepository reviewRepository, ReviewMapper reviewMapper) {
        super(Film.class, FilmWithDirectorsNameAndRatingDTO.class, modelMapper);
        this.directorRepository = directorRepository;
        this.orderRepository = orderRepository;
        this.reviewRepository = reviewRepository;
        this.reviewMapper = reviewMapper;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Film.class, FilmWithDirectorsNameAndRatingDTO.class)
                .addMappings(m -> m.skip(FilmWithDirectorsNameAndRatingDTO::setDirectorIds))
                .addMappings(m -> m.skip(FilmWithDirectorsNameAndRatingDTO::setOrderIds))
                .addMappings(m -> m.skip(FilmWithDirectorsNameAndRatingDTO::setReviewIds))
                .setPostConverter(toDTOConverter());

        modelMapper.createTypeMap(FilmWithDirectorsNameAndRatingDTO.class, Film.class)
                .addMappings(m -> m.skip(Film::setDirectors))
                .addMappings(m -> m.skip(Film::setOrders))
                .addMappings(m -> m.skip(Film::setReviews))
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(FilmWithDirectorsNameAndRatingDTO source, Film destination) {
        destination.setDirectors(Collections.emptyList());
        destination.setOrders(orderRepository.findAllById(source.getOrderIds()));
        destination.setOrders(Collections.emptyList());
        destination.setReviews(reviewRepository.findAllById(source.getReviewIds()));

    }

    @Override
    protected void mapSpecificFields(Film source, FilmWithDirectorsNameAndRatingDTO destination) {
        destination.setDirectorIds(getDirIds(source));
        destination.setOrderIds(getOrdIds(source));
        destination.setReviewIds(getRevIds(source));
        destination.setRating(getRating(source));
    }

    private List<Long> getDirIds(Film source) {
        return Objects.isNull(source) || Objects.isNull(source.getDirectors()) ?
                Collections.emptyList() :
                source.getDirectors()
                        .stream()
                        .map(GenericModel::getId)
                        .collect(Collectors.toList());
    }

    private List<Long> getOrdIds(Film source) {
        return Objects.isNull(source) || Objects.isNull(source.getOrders()) ?
                Collections.emptyList() :
                source.getOrders()
                        .stream()
                        .map(GenericModel::getId)
                        .collect(Collectors.toList());
    }

    private List<Long> getRevIds(Film source) {
        return Objects.isNull(source) || Objects.isNull(source.getReviews()) ?
                Collections.emptyList() :
                source.getReviews()
                        .stream()
                        .map(GenericModel::getId)
                        .collect(Collectors.toList());
    }

    public Double getRating(Film film) {
        List<ReviewDTO> reviews = reviewMapper.toDTOs(film.getReviews());
        if (reviews.isEmpty()) {
            return 0.0;
        } else {
            Double sum = reviews.stream().map(ReviewDTO::getRate).reduce(Double::sum).orElseThrow();
            return Math.ceil(sum * 100 / reviews.size()) / 100;
        }
    }
}
