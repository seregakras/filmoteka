package my.spring_project.mapper;

import jakarta.annotation.PostConstruct;
import my.spring_project.dto.OrderDTO;
import my.spring_project.dto.OrderWithFilmDTO;
import my.spring_project.model.Order;
import my.spring_project.repositories.FilmRepository;
import my.spring_project.repositories.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class OrderWithFilmMapper extends GenericMapper<Order, OrderWithFilmDTO> {

    private final UserRepository userRepository;
    private final FilmRepository filmRepository;
    private final FilmWithDirectorsMapper filmMapper;


    protected OrderWithFilmMapper(ModelMapper modelMapper, UserRepository userRepository, FilmRepository filmRepository, FilmWithDirectorsMapper filmMapper) {
        super(Order.class, OrderWithFilmDTO.class, modelMapper);
        this.userRepository = userRepository;
        this.filmRepository = filmRepository;
        this.filmMapper = filmMapper;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Order.class, OrderWithFilmDTO.class)
                .addMappings(m -> m.skip(OrderDTO::setUserId))
                .addMappings(m -> m.skip(OrderDTO::setFilmId))
                .setPostConverter(toDTOConverter());

        modelMapper.createTypeMap(OrderWithFilmDTO.class, Order.class)
                .addMappings(m -> m.skip(Order::setUser))
                .addMappings(m -> m.skip(Order::setFilm))
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(OrderWithFilmDTO source, Order destination) {
        destination.setFilm(filmRepository.findById(source.getFilmId()).orElseThrow());
        destination.setUser(userRepository.findById(source.getUserId()).orElseThrow());
    }

    @Override
    protected void mapSpecificFields(Order source, OrderWithFilmDTO destination) {
        destination.setFilmId(source.getFilm().getId());
        destination.setUserId(source.getUser().getId());
        destination.setOrderFilm(filmMapper.toDTO(source.getFilm()));
    }
}
