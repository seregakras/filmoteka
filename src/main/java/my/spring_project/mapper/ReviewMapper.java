package my.spring_project.mapper;

import jakarta.annotation.PostConstruct;
import my.spring_project.dto.ReviewDTO;
import my.spring_project.model.Review;
import my.spring_project.repositories.FilmRepository;
import my.spring_project.repositories.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ReviewMapper extends GenericMapper<Review, ReviewDTO> {

    private final UserRepository userRepository;
    private final FilmRepository filmRepository;


    protected ReviewMapper(ModelMapper modelMapper, UserRepository userRepository, FilmRepository filmRepository) {
        super(Review.class, ReviewDTO.class, modelMapper);
        this.userRepository = userRepository;
        this.filmRepository = filmRepository;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Review.class, ReviewDTO.class)
                .addMappings(m -> m.skip(ReviewDTO::setUserId))
                .addMappings(m -> m.skip(ReviewDTO::setFilmId))
                .setPostConverter(toDTOConverter());

        modelMapper.createTypeMap(ReviewDTO.class, Review.class)
                .addMappings(m -> m.skip(Review::setUser))
                .addMappings(m -> m.skip(Review::setFilm))
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(ReviewDTO source, Review destination) {
        destination.setFilm(filmRepository.findById(source.getFilmId()).orElseThrow());
        destination.setUser(userRepository.findById(source.getUserId()).orElseThrow());
    }

    @Override
    protected void mapSpecificFields(Review source, ReviewDTO destination) {
        destination.setFilmId(source.getFilm().getId());
        destination.setUserId(source.getUser().getId());
    }
}
