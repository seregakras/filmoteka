package my.spring_project.mapper;

import jakarta.annotation.PostConstruct;
import my.spring_project.dto.FilmDTO;
import my.spring_project.model.Film;
import my.spring_project.model.GenericModel;
import my.spring_project.repositories.DirectorRepository;
import my.spring_project.repositories.OrderRepository;
import my.spring_project.repositories.ReviewRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class FilmMapper extends GenericMapper<Film, FilmDTO> {

    private final DirectorRepository directorRepository;
    private final OrderRepository orderRepository;
    private final ReviewRepository reviewRepository;

    protected FilmMapper(ModelMapper modelMapper, DirectorRepository directorRepository,
                         OrderRepository orderRepository, ReviewRepository reviewRepository) {
        super(Film.class, FilmDTO.class, modelMapper);
        this.directorRepository = directorRepository;
        this.orderRepository = orderRepository;
        this.reviewRepository = reviewRepository;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Film.class, FilmDTO.class)
                .addMappings(m -> m.skip(FilmDTO::setDirectorIds))
                .addMappings(m -> m.skip(FilmDTO::setOrderIds))
                .addMappings(m ->m.skip(FilmDTO::setReviewIds))
                .setPostConverter(toDTOConverter());

        modelMapper.createTypeMap(FilmDTO.class, Film.class)
                .addMappings(m -> m.skip(Film::setDirectors))
                .addMappings(m -> m.skip(Film::setOrders))
                .addMappings(m -> m.skip(Film::setReviews))
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(FilmDTO source, Film destination) {
        if(!Objects.isNull(source.getDirectorIds())) {
            destination.setDirectors(directorRepository.findAllById(source.getDirectorIds()));
        }
        else {
            destination.setDirectors(Collections.emptyList());
        }
        if(!Objects.isNull(source.getOrderIds())) {
            destination.setOrders(orderRepository.findAllById(source.getOrderIds()));
        }
        else {
            destination.setOrders(Collections.emptyList());
        }
        if(!Objects.isNull(source.getReviewIds())) {
            destination.setReviews(reviewRepository.findAllById(source.getReviewIds()));
        }
        else {
            destination.setReviews(Collections.emptyList());
        }
    }

    @Override
    protected void mapSpecificFields(Film source, FilmDTO destination) {
        destination.setDirectorIds(getDirIds(source));
        destination.setOrderIds(getOrdIds(source));
        destination.setReviewIds(getRevIds(source));
    }

    private List<Long> getDirIds(Film source) {
        return Objects.isNull(source) || Objects.isNull(source.getDirectors())?
                Collections.emptyList() :
                source.getDirectors()
                        .stream()
                        .map(GenericModel::getId)
                        .collect(Collectors.toList());
    }

    private List<Long> getOrdIds(Film source) {
        return Objects.isNull(source) || Objects.isNull(source.getOrders())?
                Collections.emptyList() :
                source.getOrders()
                        .stream()
                        .map(GenericModel::getId)
                        .collect(Collectors.toList());
    }

    private List<Long> getRevIds(Film source) {
        return Objects.isNull(source) || Objects.isNull(source.getReviews())?
                Collections.emptyList() :
                source.getReviews()
                        .stream()
                        .map(GenericModel::getId)
                        .collect(Collectors.toList());
    }
}
