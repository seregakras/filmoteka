package my.spring_project.mapper;

import jakarta.annotation.PostConstruct;
import my.spring_project.dto.OrderDTO;
import my.spring_project.model.Order;
import my.spring_project.repositories.FilmRepository;
import my.spring_project.repositories.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class OrderMapper extends GenericMapper<Order, OrderDTO> {

    private final UserRepository userRepository;
    private final FilmRepository filmRepository;


    protected OrderMapper(ModelMapper modelMapper, UserRepository userRepository, FilmRepository filmRepository) {
        super(Order.class, OrderDTO.class, modelMapper);
        this.userRepository = userRepository;
        this.filmRepository = filmRepository;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Order.class, OrderDTO.class)
                .addMappings(m -> m.skip(OrderDTO::setUserId))
                .addMappings(m -> m.skip(OrderDTO::setFilmId))
                .setPostConverter(toDTOConverter());

        modelMapper.createTypeMap(OrderDTO.class, Order.class)
                .addMappings(m -> m.skip(Order::setUser))
                .addMappings(m -> m.skip(Order::setFilm))
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(OrderDTO source, Order destination) {
        destination.setFilm(filmRepository.findById(source.getFilmId()).orElseThrow());
        destination.setUser(userRepository.findById(source.getUserId()).orElseThrow());
    }

    @Override
    protected void mapSpecificFields(Order source, OrderDTO destination) {
        destination.setFilmId(source.getFilm().getId());
        destination.setUserId(source.getUser().getId());
    }
}
