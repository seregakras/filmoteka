package my.spring_project.mapper;

import jakarta.annotation.PostConstruct;
import my.spring_project.dto.UserDTO;
import my.spring_project.model.GenericModel;
import my.spring_project.model.User;
import my.spring_project.repositories.OrderRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class UserMapper extends GenericMapper<User, UserDTO> {

    private final OrderRepository orderRepository;

    protected UserMapper(ModelMapper modelMapper, OrderRepository orderRepository) {
        super(User.class, UserDTO.class, modelMapper);
        this.orderRepository = orderRepository;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(User.class, UserDTO.class)
                .addMappings(m -> m.skip(UserDTO::setOrderIds))
                .setPostConverter(toDTOConverter());

        modelMapper.createTypeMap(UserDTO.class, User.class)
                .addMappings(m -> m.skip(User::setOrders))
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(UserDTO source, User destination) {
        if(!Objects.isNull(source.getOrderIds())) {
            destination.setOrders(orderRepository.findAllById(source.getOrderIds()));
        }
        else {
            destination.setOrders(Collections.emptyList());
        }
    }

    @Override
    protected void mapSpecificFields(User source, UserDTO destination) {
        destination.setOrderIds(getIds(source));
    }

    private List<Long> getIds(User source) {
        return Objects.isNull(source) || Objects.isNull(source.getOrders())?
                Collections.emptyList() :
                source.getOrders()
                        .stream()
                        .map(GenericModel::getId)
                        .collect(Collectors.toList());
    }
}
