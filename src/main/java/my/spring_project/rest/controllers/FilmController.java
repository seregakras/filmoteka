package my.spring_project.rest.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import my.spring_project.dto.FilmDTO;
import my.spring_project.model.Film;
import my.spring_project.service.FilmService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/rest/films")
@Tag(name = "Фильмы", description = "Контроллер для работы с фильмами")
public class FilmController extends GenericController<Film, FilmDTO> {

    protected FilmController(FilmService filmService) {
        super(filmService);
    }

    @PreAuthorize(value = "hasRole('ADMIN')")
    @PostMapping(value = "/addDirector",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(method = "addDirector", description = "Добавить режиссера к фильму")
    public ResponseEntity<FilmDTO> addFilm(@RequestParam(value = "filmId") Long filmId,
                                            @RequestParam(value = "directorId") Long directorId) {
        return ResponseEntity.status(HttpStatus.OK).body(((FilmService)genericService).addDirector(filmId, directorId));
    }

}
