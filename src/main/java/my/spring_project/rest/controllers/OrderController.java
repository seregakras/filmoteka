package my.spring_project.rest.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import my.spring_project.dto.FilmDTO;
import my.spring_project.dto.OrderDTO;
import my.spring_project.model.Order;
import my.spring_project.service.OrderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/orders")
@Tag(name = "Заказы", description = "Контроллер для работы с заказами")
@SecurityRequirement(name = "Bearer Authentication")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class OrderController extends GenericController<Order, OrderDTO> {

    public OrderController(OrderService orderService) {
        super(orderService);
    }

    @PostMapping(value = "/getFilmsOfUser",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(method = "getFilmsOfUser", description = "Показать фильмы у пользователя")
    public ResponseEntity<List<FilmDTO>> getFilmsOfUser(@RequestParam(value = "userId") Long userId) {

        return ResponseEntity.status(HttpStatus.OK).body(((OrderService)genericService).getFilmsOfUser(userId));
    }
}
