package my.spring_project.rest.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import my.spring_project.dto.DirectorDTO;
import my.spring_project.model.Director;
import my.spring_project.service.DirectorService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/rest/directors")
@Tag(name = "Режиссеры", description = "Контроллер для работы с режиссерами")
public class DirectorController extends GenericController<Director, DirectorDTO> {

    protected DirectorController(DirectorService directorService) {
        super(directorService);
    }

    @PreAuthorize(value = "hasRole('ADMIN')")
    @PostMapping(value = "/addFilm",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(method = "addFilm", description = "Добавить фильм к режиссеру")
    public ResponseEntity<DirectorDTO> addFilm(@RequestParam(value = "directorId") Long directorId,
                                        @RequestParam(value = "filmId") Long filmId) {
            return ResponseEntity.status(HttpStatus.OK).body(((DirectorService)genericService).addFilm(directorId,filmId));
        }
}
