package my.spring_project.rest.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import my.spring_project.config.jwt.JWTTokenUtil;
import my.spring_project.dto.LoginDTO;
import my.spring_project.dto.UserDTO;
import my.spring_project.model.User;
import my.spring_project.service.UserService;
import my.spring_project.service.userdetail.CustomUserDetailsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping(value = "/rest/users")
@Tag(name = "Пользователи", description = "Контроллер для работы с пользователями")
public class UserController extends GenericController<User, UserDTO> {

    private final JWTTokenUtil jwtTokenUtil;
    private final CustomUserDetailsService customUserDetailsService;
    private final UserService userService;

    protected UserController(UserService userService,
                             JWTTokenUtil jwtTokenUtil,
                             CustomUserDetailsService customUserDetailsService) {
        super(userService);
        this.jwtTokenUtil = jwtTokenUtil;
        this.customUserDetailsService = customUserDetailsService;
        this.userService = userService;
    }

    @PostMapping(value = "/addRole",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(method = "addRole", description = "Добавить роль к пользователю")
    public ResponseEntity<UserDTO> addRole(@RequestParam(value = "roleId") Long roleId,
                                        @RequestParam(value = "userId") Long userId) {

        return ResponseEntity.status(HttpStatus.OK).body(((UserService)genericService).addRole(userId,roleId));
    }

    @PostMapping(value = "/auth")
    public ResponseEntity<Map<String, Object>> auth(@RequestBody LoginDTO loginDTO) {
        Map<String, Object> response = new HashMap<>();
        UserDetails foundUser = customUserDetailsService.loadUserByUsername(loginDTO.getLogin());
        log.info("foundUser: {}", foundUser.toString());
        if (!userService.checkPassword(loginDTO.getPassword(), foundUser)) {
            response.put("error", "Ошибка авторизации!\nНеправильный пароль");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                    .body(response);
        }
        String token = jwtTokenUtil.generateToken(foundUser);
        log.info(token);
        response.put("token", token);
        response.put("username", foundUser.getUsername());
        response.put("role", foundUser.getAuthorities());
        return ResponseEntity.ok(response);
    }
}
