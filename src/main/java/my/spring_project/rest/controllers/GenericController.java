package my.spring_project.rest.controllers;

import io.swagger.v3.oas.annotations.Operation;
import my.spring_project.dto.GenericDTO;
import my.spring_project.model.GenericModel;
import my.spring_project.service.GenericService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public abstract class GenericController<T extends GenericModel, N extends GenericDTO> {

    protected final GenericService<T, N> genericService;

    protected GenericController(GenericService<T, N> genericService) {
        this.genericService = genericService;
    }


    @GetMapping(value = "/getAll",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(method = "getAll", description = "Получить все записи")
    public ResponseEntity<List<N>> getAll() {
        return ResponseEntity.status(HttpStatus.OK).body(genericService.listAll());
    }

    @GetMapping(value = "/getOneById",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(method = "getOneById", description = "Получить одну запись по ID")
    public ResponseEntity<N> getOneById(@RequestParam(value = "id") Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(genericService.getOne(id));
    }

    @PostMapping(value = "/add",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(method = "add", description = "Добавить запись")
    public ResponseEntity<N> add(@RequestBody N newDTO) {
        return ResponseEntity.status(HttpStatus.CREATED).body(genericService.create(newDTO));
    }

    @PutMapping(value = "/update",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(method = "update", description = "Изменить запись")
    public ResponseEntity<N> update(@RequestBody N updateDTO,
                                    @RequestParam(value = "id") Long id) {
        updateDTO.setId(id);
        genericService.create(updateDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(updateDTO);
    }

    @DeleteMapping(value = "/delete")
    @Operation(method = "delete", description = "Удалить запись")
    public void  deleteSoft(@RequestParam(value = "id") Long id) {
        genericService.deleteSoft(id);
    }

    @DeleteMapping(value = "/delete/hard")
    @Operation(method = "delete", description = "Удалить запись")
    public void  delete(@RequestParam(value = "id") Long id) {
        genericService.delete(id);
    }

    @PutMapping(value = "/restore")
    @Operation(method = "delete", description = "Восстановить запись")
    public void  restore(@RequestParam(value = "id") Long id) {
        genericService.restore(id);
    }

}
