package my.spring_project.rest.controllers;

import io.swagger.v3.oas.annotations.tags.Tag;
import my.spring_project.dto.RoleDTO;
import my.spring_project.model.Role;
import my.spring_project.service.RoleService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/rest/roles")
@Tag(name = "Роли", description = "Контроллер для работы с ролями")
public class RoleController extends GenericController<Role, RoleDTO> {


    protected RoleController(RoleService roleService) {
        super(roleService);
    }
}
