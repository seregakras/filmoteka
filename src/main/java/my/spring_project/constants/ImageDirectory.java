package my.spring_project.constants;

public interface ImageDirectory {

    String IMAGE_UPLOAD_DIRECTORY = "images";
}
