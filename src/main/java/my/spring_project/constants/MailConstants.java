package my.spring_project.constants;

import lombok.Getter;

@Getter
public enum MailConstants {

    MAIL_SUBJECT_FOR_REGISTRATION("Регистрация сайте Онлайн Фильмотеки"),

    MAIL_MESSAGE_FOR_REGISTRATION("""
            Здравствуйте! Вы только что зарегистрировались на сайте Онлайн фильмотеки.\
            Свои регистрационные данные Вы можете посмотреть в\s"""),

    MAIL_SUBJECT_FOR_REMEMBER_PASSWORD("Восстановление пароля на сайте Онлайн Фильмотеки"),

    MAIL_MESSAGE_FOR_REMEMBER_PASSWORD("""
            Добрый день. Вы получили это письмо, так как с вашего аккаунта была отправлена заявка на восстановление пароля.\
            Для восстановления пароля перейдите по ссылке:\s""");

    private final String description;

    MailConstants(String description) {
        this.description = description;
    }

}
