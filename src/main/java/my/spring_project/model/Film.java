package my.spring_project.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "films")
@SequenceGenerator(name = "default_generator", sequenceName = "films_seq", allocationSize = 1)
public class Film extends GenericModel {

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "premier_year", nullable = false)
    private Integer premierYear;

    @Column(name = "country")
    private String country;

    @Column(name = "genre", nullable = false)
    private Genre genre;

    @Column(name = "img")
    private String srcImage = "/img/No-Image.png";

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "directors_films",
            joinColumns = @JoinColumn(name = "film_id"),
            foreignKey = @ForeignKey(name = "FK_FILMS_DIRECTORS"),
            inverseJoinColumns = @JoinColumn(name = "director_id"),
            inverseForeignKey = @ForeignKey(name = "FK_DIRECTORS_FILMS")
    )
    List<Director> directors;

    @OneToMany(mappedBy = "film", fetch = FetchType.EAGER)
    List<Order> orders;

    @OneToMany(mappedBy = "film", fetch = FetchType.EAGER)
    List<Review> reviews;

}
