package my.spring_project.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "directors")
@SequenceGenerator(name = "default_generator", sequenceName = "directors_seq",allocationSize = 1)
public class Director extends GenericModel {

    @Column(name = "director_fio", nullable = false)
    private String directorFio;

    @Column(name = "position")
    private String position;

    @Column(name = "img")
    private String srcImage = "/img/No-Image.png";

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "directors_films",
            joinColumns = @JoinColumn(name = "director_id"),
            foreignKey = @ForeignKey(name = "FK_DIRECTORS_FILMS"),
            inverseJoinColumns = @JoinColumn(name = "film_id"),
            inverseForeignKey = @ForeignKey(name = "FK_FILMS_DIRECTORS")
    )
    private List<Film> films;
}
