package my.spring_project.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "reviews")
@SequenceGenerator(name = "default_generator", sequenceName = "reviews_seq",allocationSize = 1)
public class Review extends GenericModel {

    @Column(name = "text_review")
    private String textReview;

    @Column(name = "rate")
    private Double rate = 0.0;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false,
            foreignKey = @ForeignKey(name = "FK_REVIEW_USER")
    )
    private User user;

    @ManyToOne
    @JoinColumn(name = "film_id", nullable = false,
            foreignKey = @ForeignKey(name = "FK_REVIEW_FILM")
    )
    private Film film;

    @Column(name = "created_when", nullable = false)
    private LocalDateTime createdWhen;

    @Column(name = "created_by", nullable = false)
    private String createdBy;

}
