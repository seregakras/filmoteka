package my.spring_project.model;

import lombok.Getter;

@Getter
public enum Genre {
    COMEDY("Комедия"),
    FANTASY("Фантастика"),
    HORROR("Ужасы"),
    ACTION("Боевик"),
    ANIME("Аниме"),
    DRAMA("Драма"),
    CRIMINAL("Криминал"),
    CARTOON("Мультфильм"),
    OTHER("Другой жанр");

    private final String description;

    Genre(String description) {
        this.description = description;
    }
}
