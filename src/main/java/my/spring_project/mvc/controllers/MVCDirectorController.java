package my.spring_project.mvc.controllers;

import my.spring_project.dto.DirectorDTO;
import my.spring_project.dto.DirectorSearchDTO;
import my.spring_project.dto.FilmDTO;
import my.spring_project.model.Director;
import my.spring_project.service.DirectorService;
import my.spring_project.service.FilmService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/directors")
public class MVCDirectorController {

    private static final String DIRECTOR = "director";
    private static final String REDIRECT_TO_DIRECTORS = "redirect:/directors";

    private final DirectorService directorService;
    private final FilmService filmService;

    public MVCDirectorController(DirectorService directorService, FilmService filmService) {
        this.directorService = directorService;
        this.filmService = filmService;
    }

    @GetMapping("")
    public String getAll(@RequestParam(value = "page", defaultValue = "1") int page,
                         @RequestParam(value = "size", defaultValue = "5") int pageSize,
                         Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "directorFio"));
        Page<Director> directors = directorService.getAll(pageRequest);
        model.addAttribute("directors", directors);
        return "directors/allDirectors";
    }

    @GetMapping("/{id}")
    public String getOne(@PathVariable Long id,
                         Model model) {
        DirectorDTO directorDTO = directorService.getOne(id);
        List<FilmDTO> films = new ArrayList<>();
        for (var filmId: directorDTO.getFilmIds()) {
            films.add(filmService.getOne(filmId));
        }
        model.addAttribute(DIRECTOR, directorDTO);
        model.addAttribute("films", films);
        return "directors/viewDirector";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("add-director")
    public String create() {
        return "directors/addDirector";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/add-director")
    public String create(@ModelAttribute("directorForm") DirectorDTO directorDTO,
                         @RequestParam MultipartFile file) {
        if (file != null && file.getSize() > 0) {
            directorService.create(directorDTO, file);
        } else {
            directorService.create(directorDTO);
        }
        return REDIRECT_TO_DIRECTORS;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        directorService.deleteSoft(id);
        return REDIRECT_TO_DIRECTORS;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/recovery/{id}")
    public String recovery(@PathVariable Long id) {
        directorService.restore(id);
        return REDIRECT_TO_DIRECTORS;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id,
                         Model model) {
        DirectorDTO director = directorService.getOne(id);
        model.addAttribute(DIRECTOR, director);
        return "directors/updateDirector";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/update")
    public String update(@ModelAttribute("directorForm") DirectorDTO directorDTO,
                         @RequestPart MultipartFile file) {
        directorDTO.setIsDeleted(directorService.getOne(directorDTO.getId()).getIsDeleted());
        directorDTO.setFilmIds(directorService.getOne(directorDTO.getId()).getFilmIds());
        if (file != null && file.getSize() > 0) {
            directorService.update(directorDTO, file);
        } else {
            directorService.update(directorDTO);
        }
        return REDIRECT_TO_DIRECTORS;
    }


    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/add-film-to-director/{id}")
    public String addFilmToDirector(Model model, @PathVariable Long id) {
        DirectorDTO directorDTO = directorService.getOne(id);
        List<FilmDTO> films= filmService.listAll();
        model.addAttribute("films", films);
        model.addAttribute(DIRECTOR, directorDTO);
        return "directors/addFilmToDirector";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/add-film-to-director")
    public String addFilmToDirector(@RequestParam Long filmId, @RequestParam Long directorId) {
        if(filmId != 0) {
            directorService.addFilm(directorId, filmId);
        }
        return REDIRECT_TO_DIRECTORS;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/del-film/{directorId}/{filmId}")
    public String delFilmOfDirector(@PathVariable Long directorId, @PathVariable Long filmId) {
        directorService.delFilm(directorId, filmId);
        return REDIRECT_TO_DIRECTORS;
    }

    @PostMapping("/search")
    public String searchDirectors(@RequestParam(value = "page", defaultValue = "1") int page,
                              @RequestParam(value = "size", defaultValue = "5") int pageSize,
                              @ModelAttribute("directorSearchForm") DirectorSearchDTO directorSearchDTO,
                              Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "directorFio"));
        Page<Director> directors = directorService.findDirectors(directorSearchDTO, pageRequest);
        model.addAttribute("directors", directors);
        return "directors/allDirectors";
    }
}
