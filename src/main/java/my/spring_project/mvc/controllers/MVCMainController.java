package my.spring_project.mvc.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MVCMainController {

    @RequestMapping("/")
    public String index() {
        return "index";
    }
}