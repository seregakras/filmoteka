package my.spring_project.mvc.controllers;

import my.spring_project.dto.DirectorDTO;
import my.spring_project.dto.FilmDTO;
import my.spring_project.dto.FilmWithDirectorsNameAndRatingDTO;
import my.spring_project.model.Genre;
import my.spring_project.service.DirectorService;
import my.spring_project.service.FilmService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/films")
public class MVCFilmController {
    private static final String REDIRECT_TO_FILMS = "redirect:/films";

    private final FilmService filmService;
    private final DirectorService directorService;

    public MVCFilmController(FilmService filmService, DirectorService directorService) {
        this.filmService = filmService;
        this.directorService = directorService;
    }

    @GetMapping("")
    public String getAll(@RequestParam(value = "page", defaultValue = "1") int page,
                         @RequestParam(value = "size", defaultValue = "5") int pageSize,
                         Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "title"));
        Page<FilmWithDirectorsNameAndRatingDTO> films = filmService.getAllFilmsWithDirectors(pageRequest);
        model.addAttribute("films", films);
        return "films/allFilms";
    }

    @GetMapping("/{id}")
    public String getOne(@PathVariable Long id,
                          Model model) {
        FilmWithDirectorsNameAndRatingDTO filmDTO = filmService.searchFilmWithDirectorsAndRating(id);
        List<DirectorDTO> directors = new ArrayList<>();
        for (var directorId: filmDTO.getDirectorIds()) {
            directors.add(directorService.getOne(directorId));
        }
        model.addAttribute("film", filmDTO);
        model.addAttribute("directors", directors);
        return "films/viewFilm";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/add-film")
    public String create(Model model) {
        List<Genre> genres = List.of(my.spring_project.model.Genre.values());
        model.addAttribute("genres", genres);
        return "films/addFilm";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/add-film")
    public String create(@ModelAttribute("filmForm") FilmDTO filmDTO,
                         @RequestParam MultipartFile file) {
        if (file != null && file.getSize() > 0) {
            filmService.create(filmDTO, file);
        } else {
            filmService.create(filmDTO);
        }
        return REDIRECT_TO_FILMS;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        filmService.deleteSoft(id);
        return REDIRECT_TO_FILMS;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/recovery/{id}")
    public String recovery(@PathVariable Long id) {
        filmService.restore(id);
        return REDIRECT_TO_FILMS;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id,
                               Model model) {
        FilmDTO film = filmService.getOne(id);
        model.addAttribute("film", film);
        return "films/updateFilm";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/update")
    public String update(@ModelAttribute("filmForm") FilmDTO filmDTO,
                         @RequestPart MultipartFile file) {
        filmDTO.setIsDeleted(filmService.getOne(filmDTO.getId()).getIsDeleted());
        filmDTO.setOrderIds(filmService.getOne(filmDTO.getId()).getOrderIds());
        filmDTO.setDirectorIds(filmService.getOne(filmDTO.getId()).getDirectorIds());
        filmDTO.setReviewIds(filmService.getOne(filmDTO.getId()).getReviewIds());
        if (file != null && file.getSize() > 0) {
            filmService.update(filmDTO, file);
        } else {
            filmService.update(filmDTO);
        }
        return REDIRECT_TO_FILMS;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/add-director-to-film/{id}")
    public String addDirector(Model model, @PathVariable Long id) {
        FilmDTO filmDTO = filmService.getOne(id);
        List<DirectorDTO> directors= directorService.listAll();
        model.addAttribute("directors", directors);
        model.addAttribute("film", filmDTO);
        return "films/addDirectorToFilm";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/add-director-to-film")
    public String addDirector(@RequestParam Long directorId, @RequestParam Long filmId) {
        if(directorId != 0) {
            filmService.addDirector(filmId, directorId);
        }
        return REDIRECT_TO_FILMS;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/del-director/{filmId}/{directorId}")
    public String delDirector(@PathVariable Long filmId, @PathVariable Long directorId) {
        filmService.delDirector(filmId, directorId);
        return REDIRECT_TO_FILMS;
    }

    @PostMapping("/search")
    public String searchFilms(@RequestParam(value = "page", defaultValue = "1") int page,
                              @RequestParam(value = "size", defaultValue = "5") int pageSize,
                              @ModelAttribute("filmSearchForm") FilmWithDirectorsNameAndRatingDTO filmSearchDTO,
                              Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "title"));
        Page<FilmWithDirectorsNameAndRatingDTO> films = filmService.searchFilms(filmSearchDTO, pageRequest);
        model.addAttribute("films", films);
        return "films/allFilms";
    }

}
