package my.spring_project.mvc.controllers;

import jakarta.websocket.server.PathParam;
import my.spring_project.dto.UserDTO;
import my.spring_project.service.UserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Objects;

@Controller
@RequestMapping("/users")
public class MVCUserController {

    private static final String URL_REGISTRATION = "users/registration";
    private static final String REDIRECT_TO_LOGIN = "redirect:/login";

    private final UserService userService;

    public MVCUserController(UserService userService) {
        this.userService = userService;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("")
    public String getAll(Model model) {
        List<UserDTO> users = userService.listAll();
        model.addAttribute("users", users);
        return "users/allUsers";
    }

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("userForm", new UserDTO());
        return URL_REGISTRATION;
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("userForm") UserDTO userDTO,
                               BindingResult bindingResult) {
        if (userDTO.getLogin().equalsIgnoreCase("ADMIN") || userService.getUserByLogin(userDTO.getLogin()) != null) {
            bindingResult.rejectValue("login", "error.login", "Такой логин существует!");
            return URL_REGISTRATION;
        } else if (userService.getUserByEmail(userDTO.getEmail()) != null) {
            bindingResult.rejectValue("email", "error.email", "Такой e-mail существует!");
            return URL_REGISTRATION;
        } else {
            userService.create(userDTO);
            userService.sendRegistrationToEmail(userDTO);
            return REDIRECT_TO_LOGIN;
        }
    }

    @GetMapping("/remember-password")
    public String rememberPassword() {
        return "users/rememberPassword";
    }

    @PostMapping("/remember-password")
    public String rememberPassword(@ModelAttribute("changePasswordForm") UserDTO userDTO) {
        userDTO = userService.getUserByEmail(userDTO.getEmail());
        if (Objects.isNull(userDTO)) {
            return "error";
        } else {
            userService.sendChangePasswordEmail(userDTO);
            return REDIRECT_TO_LOGIN;
        }
    }

    @GetMapping("/change-password")
    public String changePassword(@PathParam(value = "uuid") String uuid,
                                 Model model) {
        model.addAttribute("uuid", uuid);
        return "users/changePassword";
    }

    @PostMapping("/change-password")
    public String changePassword(@PathParam(value = "uuid") String uuid,
                                 @ModelAttribute("changePasswordForm") UserDTO userDTO) {
        userService.changePassword(uuid, userDTO.getPassword());
        return REDIRECT_TO_LOGIN;
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping("/profile")
    public String viewProfile(Model model) {
        UserDTO user = userService.getUserByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
        model.addAttribute("user", user);
        model.addAttribute("profileForm", new UserDTO());
        return "users/profile";
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping("/update")
    public String updateUser(@ModelAttribute("profileForm") UserDTO userDTO) {
        userService.update(userDTO);
        return "redirect:/";
    }
}
