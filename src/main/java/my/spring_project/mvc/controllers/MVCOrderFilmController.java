package my.spring_project.mvc.controllers;

import my.spring_project.dto.OrderDTO;
import my.spring_project.dto.OrderWithFilmDTO;
import my.spring_project.dto.UserDTO;
import my.spring_project.service.FilmService;
import my.spring_project.service.OrderService;
import my.spring_project.service.UserService;
import my.spring_project.service.userdetail.CustomUserDetails;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@PreAuthorize("hasRole('USER')")
@RequestMapping("/orders")
public class MVCOrderFilmController {

    private static final String RENT_DATE = "rentDate";
    private static final String ORDERS = "orders";

    private final FilmService filmService;
    private final OrderService orderService;
    private final UserService userService;

    public MVCOrderFilmController(FilmService filmService, OrderService orderService, UserService userService) {
        this.filmService = filmService;
        this.orderService = orderService;
        this.userService = userService;
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping("")
    public String getAll(@RequestParam(value = "page", defaultValue = "1") int page,
                         @RequestParam(value = "size", defaultValue = "5") int pageSize,
                         Model model) {
        Long id = ((CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId().longValue();
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.DESC, RENT_DATE));
        Page<OrderWithFilmDTO> orders = orderService.getAllActiveOrdersByUserId(id, pageRequest);
        model.addAttribute(ORDERS, orders);
        return "orders/myOrders";
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping("/old")
    public String getOldOrders(@RequestParam(value = "page", defaultValue = "1") int page,
                                @RequestParam(value = "size", defaultValue = "5") int pageSize,
                                Model model) {
        Long id = ((CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId().longValue();
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.DESC, RENT_DATE));
        Page<OrderWithFilmDTO> orders = orderService.getAllOrdersByUserId(id, pageRequest);
        model.addAttribute(ORDERS, orders);
        boolean old = true;
        model.addAttribute("old", old);
        return "orders/myOrders";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/{id}")
    public String getUserOrders(@PathVariable Long id,
                                @RequestParam(value = "page", defaultValue = "1") int page,
                                @RequestParam(value = "size", defaultValue = "5") int pageSize,
                                Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.DESC, RENT_DATE));
        Page<OrderWithFilmDTO> orders = orderService.getAllOrdersByUserId(id, pageRequest);
        UserDTO userDTO = userService.getOne(id);
        model.addAttribute(ORDERS, orders);
        model.addAttribute("user", userDTO);
        return "orders/viewUserOrders";
    }

    @GetMapping("/film/{id}")
    public String create(@PathVariable Long id,
                            Model model) {
        model.addAttribute("film", filmService.getOne(id));
        return "orders/orderFilm";
    }

    @PostMapping("/order-film")
    public String create(@ModelAttribute("orderFilmForm") OrderDTO orderDTO) {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.
                getContext().getAuthentication().getPrincipal();
        orderDTO.setUserId(Long.valueOf(customUserDetails.getId()));
        orderService.orderFilm(orderDTO);
        return "redirect:/orders";
    }

    @GetMapping("/return/{id}")
    public String returnFilm(@PathVariable Long id) {
        orderService.returnFilm(id);
        return "redirect:/orders";
    }


}
