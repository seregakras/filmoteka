package my.spring_project.mvc.controllers;

import my.spring_project.dto.FilmDTO;
import my.spring_project.dto.ReviewDTO;
import my.spring_project.dto.ReviewWithFilmDTO;
import my.spring_project.dto.UserDTO;
import my.spring_project.service.FilmService;
import my.spring_project.service.ReviewService;
import my.spring_project.service.UserService;
import my.spring_project.service.userdetail.CustomUserDetails;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@Controller
@RequestMapping("/reviews")
public class MVCReviewController {

    private final ReviewService reviewService;
    private final FilmService filmService;
    private final UserService userService;


    public MVCReviewController(ReviewService reviewService, FilmService filmService, UserService userService) {
        this.reviewService = reviewService;
        this.filmService = filmService;
        this.userService = userService;
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping("")
    public String getAll(Model model) {
        Long id = ((CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId().longValue();
        List<ReviewWithFilmDTO> reviews = reviewService.getAllReviewsByUserId(id);
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.
                getContext().getAuthentication().getPrincipal();
        UserDTO user = userService.getOne(Long.valueOf(customUserDetails.getId()));
        model.addAttribute("user", user);
        model.addAttribute("reviews", reviews);
        return "reviews/myReviews";
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping("/addReview/{id}")
    public String addReview(@PathVariable Long id, Model model) {
        FilmDTO film = filmService.getOne(id);
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.
                getContext().getAuthentication().getPrincipal();
        UserDTO user = userService.getOne(Long.valueOf(customUserDetails.getId()));
        model.addAttribute("film", film);
        model.addAttribute("user", user);
        return "reviews/addReview";
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping("/addReview")
    public String addReview(@ModelAttribute("reviewForm")ReviewDTO reviewDTO) {
        reviewService.add(reviewDTO);
        return "redirect:/orders";
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping("/update/{id}")
    public String updateReview(@PathVariable Long id, Model model) {
        ReviewDTO review = reviewService.getOne(id);
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.
                getContext().getAuthentication().getPrincipal();
        UserDTO user = userService.getOne(Long.valueOf(customUserDetails.getId()));
        FilmDTO film = filmService.getOne(review.getFilmId());
        model.addAttribute("review", review);
        model.addAttribute("user", user);
        model.addAttribute("film", film);
        return "reviews/updateReview";
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping("/update")
    public String updateReview(@ModelAttribute("reviewForm")ReviewDTO reviewDTO) {
        reviewDTO.setUserId(reviewService.getOne(reviewDTO.getId()).getUserId());
        reviewDTO.setFilmId(reviewService.getOne(reviewDTO.getId()).getFilmId());
        reviewDTO.setCreatedBy(reviewService.getOne(reviewDTO.getId()).getCreatedBy());
        reviewDTO.setCreatedWhen(LocalDateTime.now());
        reviewService.update(reviewDTO);
        return "redirect:/reviews";
    }

    @GetMapping("/film/{id}")
    public String filmReview(@PathVariable Long id, Model model) {
        FilmDTO film = filmService.getOne(id);
        List<ReviewDTO> reviews = reviewService.getFilmReviewsOrderByCreatedWhen(id);
        model.addAttribute("film", film);
        model.addAttribute("reviews", reviews);
        return "reviews/filmReviews";
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping("/delete/{id}")
    public String delReview(@PathVariable Long id) {
        reviewService.delete(id);
        return "redirect:/reviews";
    }

}