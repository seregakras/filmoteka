package my.spring_project.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@ToString
@NoArgsConstructor
@Getter
@Setter
public class RoleDTO extends GenericDTO{

    private String title;
    private String description;
    List<Long> userIds;
}
