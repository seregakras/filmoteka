package my.spring_project.dto;

import lombok.Data;
import my.spring_project.model.Genre;

@Data
public class FilmSearchDTO {

    private String title;
    private String country;
    private Genre genre;
}
