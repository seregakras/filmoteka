package my.spring_project.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@ToString
@NoArgsConstructor
@Getter
@Setter
public class DirectorDTO extends GenericDTO {
    private String directorFio;
    private String position;
    private String srcImage;
    private List<Long> filmIds;

    public DirectorDTO(String directorFio, String position, String srcImage, List<Long> filmIds) {
        this.directorFio = directorFio;
        this.position = position;
        this.srcImage = srcImage;
        this.filmIds = filmIds;
    }
}
