package my.spring_project.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public abstract class GenericDTO {

    private Long id;
    private Boolean isDeleted;

}
