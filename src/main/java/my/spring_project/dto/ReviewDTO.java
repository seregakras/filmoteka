package my.spring_project.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@ToString
@NoArgsConstructor
@Getter
@Setter
public class ReviewDTO extends GenericDTO {

    private Double rate;
    private String textReview;
    private Long userId;
    private Long filmId;
    private LocalDateTime createdWhen;
    private String createdBy;

}
