package my.spring_project.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import my.spring_project.model.Review;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@ToString
@NoArgsConstructor
@Getter
@Setter
public class LoginDTO {

    private String login;
    private String password;

}
