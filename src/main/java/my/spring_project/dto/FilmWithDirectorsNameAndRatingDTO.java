package my.spring_project.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@ToString
@NoArgsConstructor
@Getter
@Setter
public class FilmWithDirectorsNameAndRatingDTO extends FilmDTO {

    private List<DirectorDTO> directors;
    private Double rating = 0.0;

}
