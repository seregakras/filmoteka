package my.spring_project.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@NoArgsConstructor
@Getter
@Setter
public class ReviewWithFilmDTO extends ReviewDTO {

    private FilmDTO reviewFilm;

}
