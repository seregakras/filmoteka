package my.spring_project.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import java.time.LocalDate;

@ToString
@NoArgsConstructor
@Getter
@Setter
public class OrderDTO extends GenericDTO {

    private Long userId;
    private Long filmId;
    private LocalDate rentDate;
    private Integer rentPeriod;
    private Boolean purchase;
}
