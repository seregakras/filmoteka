package my.spring_project.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import my.spring_project.model.Genre;

import java.util.List;

@ToString
@NoArgsConstructor
@Getter
@Setter
public class FilmDTO extends GenericDTO {

    private String title;
    private Integer premierYear;
    private String country;
    private Genre genre;
    private String srcImage;
    List<Long> directorIds;
    List<Long> orderIds;
    List<Long> reviewIds;

    public FilmDTO(String title, Integer premierYear, String country, Genre genre, String srcImage,
                   List<Long> directorIds, List<Long> orderIds, List<Long> reviewIds) {
        this.title = title;
        this.premierYear = premierYear;
        this.country = country;
        this.genre = genre;
        this.srcImage = srcImage;
        this.directorIds = directorIds;
        this.orderIds = orderIds;
        this.reviewIds = reviewIds;
    }
}
