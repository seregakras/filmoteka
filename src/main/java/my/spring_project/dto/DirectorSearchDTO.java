package my.spring_project.dto;

import lombok.Data;

@Data
public class DirectorSearchDTO {

    private String directorFio;
    private String position;
}
