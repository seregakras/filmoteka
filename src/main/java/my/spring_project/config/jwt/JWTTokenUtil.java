package my.spring_project.config.jwt;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.function.Function;

@Component
@Slf4j
public class JWTTokenUtil {

    public static final long JWT_TOKEN_VALIDITY = 604800000;

    private static final String WORLD = "petr-java";
    private static final ObjectMapper objectMapper = new ObjectMapper();

    public String generateToken(final UserDetails payload) {
        return Jwts.builder()
                .setSubject(payload.toString())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY))
                .signWith(SignatureAlgorithm.HS512, WORLD)
                .compact();
    }

    private Boolean isTokenExpired(final String token) {

        final Date expiration = getExpirationDateFromToken(token);
        return  expiration.before(new Date());
    }

    private Date getExpirationDateFromToken(final String token) {
        return getClaimsFromToken(token, Claims::getExpiration);
    }

    public Boolean validateToken(final String token) {
        return !isTokenExpired(token);
    }

    public String getParameterFromToken(final String token, final String parameter) {

        String claims = getClaimsFromToken(token, Claims::getSubject);
        JsonNode claimJSON = null;
        try {
                claimJSON = objectMapper.readTree(claims);
        } catch (JsonProcessingException e) {
            log.error("JWTTokenUtil#getUserNameFromToken: {}", e.getMessage());
        }
        assert claimJSON != null;
        return claimJSON.isNull() ? null : claimJSON.get(parameter).asText() ;
    }

    private <T> T getClaimsFromToken(final String token, Function<Claims, T> claimsResolver) {

        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    private Claims getAllClaimsFromToken(String token) {

        return Jwts.parser().setSigningKey(WORLD).parseClaimsJws(token).getBody();
    }
}
