package my.spring_project.repositories;

import my.spring_project.model.Review;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReviewRepository extends GenericRepository<Review> {

    List<Review> findReviewsByUserIdOrderByFilm(Long id);

    List<Review> findReviewsByFilmIdOrderByCreatedWhen(Long id);
}
