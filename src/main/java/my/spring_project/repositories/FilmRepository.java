package my.spring_project.repositories;

import my.spring_project.model.Film;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmRepository extends GenericRepository<Film> {

    @Query(nativeQuery = true,
            value = """
    select * from films
        where title ilike '%' || coalesce(:title, '%') || '%'
        and country ilike '%' || coalesce(:country, '%') || '%'
        and cast(genre as char) like coalesce(:genre, '%')
    """)
    Page<Film> searchFilms(@Param(value = "title") String title,
                           @Param(value = "country") String country,
                           @Param(value = "genre") String genre,
                           Pageable pageable
                           );
}
