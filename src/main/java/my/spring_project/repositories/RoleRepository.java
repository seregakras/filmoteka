package my.spring_project.repositories;

import my.spring_project.model.Role;

public interface RoleRepository extends GenericRepository<Role>{
}
