package my.spring_project.repositories;

import my.spring_project.model.Director;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DirectorRepository extends GenericRepository<Director> {

    @Query(value = """
    select d from Director d
        where d.directorFio ilike '%' || coalesce(:directorFio, '%') || '%'
        and d.position ilike '%' || coalesce(:position, '%') || '%'
    """)
    Page<Director> searchDirectors(@Param(value = "directorFio") String directorFio,
                           @Param(value = "position") String position,
                           Pageable pageable);

}
