package my.spring_project.repositories;

import my.spring_project.model.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends GenericRepository<User> {

    User findUserByLogin(String login);
    User findUserByEmail(String email);

    User findUserByChangePasswordToken(String uuid);
}
