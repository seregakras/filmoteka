package my.spring_project.repositories;

import my.spring_project.model.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends GenericRepository<Order> {

    Page<Order> findOrdersByUserId(Long id, PageRequest pageRequest);

    Page<Order> findOrdersByUserIdAndPurchaseIsTrue(Long id, PageRequest pageRequest);
}
