package my.spring_project.service;

import my.spring_project.dto.DirectorDTO;
import my.spring_project.dto.DirectorSearchDTO;
import my.spring_project.mapper.DirectorMapper;
import my.spring_project.model.Director;
import my.spring_project.repositories.DirectorRepository;
import my.spring_project.utils.FileHelper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class DirectorService extends GenericService<Director, DirectorDTO> {

    private final DirectorRepository directorRepository;

    protected DirectorService(DirectorRepository repository, DirectorMapper mapper) {
        super(repository, mapper);
        this.directorRepository = repository;
    }

    public Page<Director> getAll(Pageable pageable) {

        Page<Director> directorsPaginated = directorRepository.findAll(pageable);
        List<Director> result = directorsPaginated.getContent();
        return new PageImpl<>(result, pageable, directorsPaginated.getTotalElements());
    }

    public DirectorDTO create(final DirectorDTO directorDTO, MultipartFile file) {
        DirectorDTO createdDirector = mapper.toDTO(directorRepository.save(mapper.toEntity(directorDTO)));
        String fileName = FileHelper.createFile(file, "director" + createdDirector.getId().toString());
        createdDirector.setSrcImage(fileName);
        return createdDirector;
    }

    public DirectorDTO update(final DirectorDTO directorDTO, MultipartFile file) {
        String fileName = FileHelper.createFile(file, "director" + directorDTO.getId().toString());
        directorDTO.setSrcImage(fileName);
        return mapper.toDTO(directorRepository.save(mapper.toEntity(directorDTO)));
    }

    public DirectorDTO addFilm(final Long directorId, final Long filmId) {
        DirectorDTO directorDTO = getOne(directorId);
        directorDTO.getFilmIds().add(filmId);
        update(directorDTO);
        return directorDTO;
    }

    public void delFilm(final Long directorId, final Long filmId) {
        DirectorDTO directorDTO = getOne(directorId);
        directorDTO.getFilmIds().remove(filmId);
        update(directorDTO);
    }

    public Page<Director> findDirectors(DirectorSearchDTO directorSearchDTO, PageRequest pageRequest) {

        Page<Director> directorsPaginated = directorRepository.searchDirectors(directorSearchDTO.getDirectorFio(),
                directorSearchDTO.getPosition(),
                pageRequest);
        List<Director> result = directorsPaginated.getContent();
        return new PageImpl<>(result, pageRequest, directorsPaginated.getTotalElements());
    }
}
