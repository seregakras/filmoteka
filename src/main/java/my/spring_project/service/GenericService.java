package my.spring_project.service;

import my.spring_project.dto.GenericDTO;
import my.spring_project.mapper.GenericMapper;
import my.spring_project.model.GenericModel;
import my.spring_project.repositories.GenericRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public abstract class GenericService<T extends GenericModel, N extends GenericDTO> {
    protected final GenericRepository<T> repository;
    protected final GenericMapper<T, N> mapper;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    protected GenericService(GenericRepository<T> repository, GenericMapper<T, N> mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public List<N> listAll() {
        return mapper.toDTOs(repository.findAll());
    }

    public N getOne(final Long id) {
        return mapper.toDTO(repository.findById(id).orElseThrow());
    }

    public N create(N newDTO) {
        return mapper.toDTO(repository.save(mapper.toEntity(newDTO)));
    }

    public N update(N updateDTO) {
        return mapper.toDTO(repository.save(mapper.toEntity(updateDTO)));
    }

    public void delete(final Long id) {
        repository.deleteById(id);
    }

    public void deleteSoft(final Long id) {
        T findObj = repository.findById(id).orElseThrow();
        findObj.setIsDeleted(true);
        repository.save(findObj);
    }

    public void restore(final Long id) {
        T findObj = repository.findById(id).orElseThrow();
        findObj.setIsDeleted(false);
        repository.save(findObj);
    }
}
