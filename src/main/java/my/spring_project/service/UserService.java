package my.spring_project.service;

import lombok.extern.slf4j.Slf4j;
import my.spring_project.dto.RoleDTO;
import my.spring_project.dto.UserDTO;
import my.spring_project.mapper.UserMapper;
import my.spring_project.model.User;
import my.spring_project.repositories.UserRepository;
import my.spring_project.utils.MailUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.UUID;

import static my.spring_project.constants.MailConstants.*;

@Service
@Slf4j
public class UserService extends GenericService<User, UserDTO> {

    protected final RoleService roleService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final JavaMailSender javaMailSender;

    @Value("${server.port}")
    private Integer serverPort;


    protected UserService(UserRepository userRepository, UserMapper userMapper,
                          RoleService roleService, BCryptPasswordEncoder bCryptPasswordEncoder, JavaMailSender javaMailSender) {
        super(userRepository, userMapper);
        this.roleService = roleService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.javaMailSender = javaMailSender;
    }

    @Override
    public UserDTO create(UserDTO newDTO) {
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId(1L);
        newDTO.setRole(roleDTO);
        newDTO.setCreatedWhen(LocalDateTime.now());
        newDTO.setPassword(bCryptPasswordEncoder.encode(newDTO.getPassword()));
        return mapper.toDTO(repository.save(mapper.toEntity(newDTO)));
    }

    @Override
    public UserDTO update(UserDTO newDTO) {
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId(1L);
        newDTO.setRole(roleDTO);
        newDTO.setCreatedWhen(LocalDateTime.now());
        return mapper.toDTO(repository.save(mapper.toEntity(newDTO)));
    }

    public UserDTO addRole(final Long userId, final Long roleId) {
        UserDTO userDTO = getOne(userId);
        userDTO.setRole(roleService.getOne(roleId));
        update(userDTO);
        return userDTO;
    }

    public UserDTO getUserByLogin(String login) {
        return mapper.toDTO(((UserRepository) repository).findUserByLogin(login));
    }

    public UserDTO getUserByEmail(String email) {
        return mapper.toDTO(((UserRepository) repository).findUserByEmail(email));
    }

    public void sendRegistrationToEmail(final UserDTO userDTO) {
        SimpleMailMessage mailMessage = MailUtils.createMailMessage(userDTO.getEmail(),
                MAIL_MESSAGE_FOR_REGISTRATION.name(),
                MAIL_MESSAGE_FOR_REGISTRATION + "<a href=''>личном кабинете</a>");
        mailMessage.setFrom("seregakras@yandex.ru");
        javaMailSender.send(mailMessage);
    }

    public void sendChangePasswordEmail(final UserDTO userDTO) {
        UUID uuid = UUID.randomUUID();
        userDTO.setChangePasswordToken(uuid.toString());
        update(userDTO);
        SimpleMailMessage mailMessage = MailUtils.createMailMessage(userDTO.getEmail(),
                MAIL_SUBJECT_FOR_REMEMBER_PASSWORD.name(),
                MAIL_MESSAGE_FOR_REMEMBER_PASSWORD + " https://petr-java.ru:" + serverPort + "/users/change-password" +
                "?uuid=" + uuid);
        mailMessage.setFrom("seregakras@yandex.ru");
        javaMailSender.send(mailMessage);
    }

    public void changePassword(final String uuid,
                               final String password) {
        UserDTO userDTO = mapper.toDTO(((UserRepository) repository).findUserByChangePasswordToken(uuid));
        userDTO.setChangePasswordToken(null);
        userDTO.setPassword(bCryptPasswordEncoder.encode(password));
        update(userDTO);
    }

    public boolean checkPassword(String password, UserDetails foundUser) {
        return bCryptPasswordEncoder.matches(password, foundUser.getPassword());
    }
}
