package my.spring_project.service;

import my.spring_project.dto.FilmDTO;
import my.spring_project.dto.FilmWithDirectorsNameAndRatingDTO;
import my.spring_project.mapper.FilmMapper;
import my.spring_project.mapper.FilmWithDirectorsMapper;
import my.spring_project.model.Film;
import my.spring_project.repositories.FilmRepository;
import my.spring_project.utils.FileHelper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class FilmService extends GenericService<Film, FilmDTO> {

    private final FilmRepository filmRepository;
    private final FilmWithDirectorsMapper filmWithDirectorsMapper;

    protected FilmService(FilmRepository repository, FilmMapper mapper, FilmWithDirectorsMapper filmWithDirectorsMapper) {
        super(repository, mapper);
        this.filmRepository = repository;
        this.filmWithDirectorsMapper = filmWithDirectorsMapper;
    }

    public FilmDTO create(final FilmDTO filmDTO, MultipartFile file) {
        FilmDTO createdFilm = mapper.toDTO(filmRepository.save(mapper.toEntity(filmDTO)));
        String fileName = FileHelper.createFile(file, "film" + createdFilm.getId().toString());
        filmDTO.setSrcImage(fileName);
        return createdFilm;
    }

    public FilmDTO update(final FilmDTO filmDTO, MultipartFile file) {
        String fileName = FileHelper.createFile(file, "film" + filmDTO.getId().toString());
        filmDTO.setSrcImage(fileName);
        return mapper.toDTO(filmRepository.save(mapper.toEntity(filmDTO)));
    }

    public Page<FilmWithDirectorsNameAndRatingDTO> getAllFilmsWithDirectors(Pageable pageable) {
        Page<Film> booksPaginated = filmRepository.findAll(pageable);
        List<FilmWithDirectorsNameAndRatingDTO> result = filmWithDirectorsMapper.toDTOs(booksPaginated.getContent());
        return new PageImpl<>(result, pageable, booksPaginated.getTotalElements());
    }

    public FilmDTO addDirector(final Long filmId, final Long directorId) {
        FilmDTO filmDTO = getOne(filmId);
        filmDTO.getDirectorIds().add(directorId);
        update(filmDTO);
        return filmDTO;
    }

    public void delDirector(final Long filmId, final Long directorId) {
        FilmDTO filmDTO = getOne(filmId);
        filmDTO.getDirectorIds().remove(directorId);
        update(filmDTO);
    }

    public Page<FilmWithDirectorsNameAndRatingDTO> searchFilms(FilmWithDirectorsNameAndRatingDTO filmSearchDTO, Pageable pageable) {

        String genre = filmSearchDTO.getGenre() != null? String.valueOf(filmSearchDTO.getGenre().ordinal()): null;

        Page<Film> filmsPaginated = filmRepository.searchFilms(filmSearchDTO.getTitle(),
                                                            filmSearchDTO.getCountry(),
                                                            genre,
                                                            pageable);
        List<FilmWithDirectorsNameAndRatingDTO> result = filmWithDirectorsMapper.toDTOs(filmsPaginated.getContent());
        return new PageImpl<>(result, pageable, filmsPaginated.getTotalElements());
    }

    public FilmWithDirectorsNameAndRatingDTO searchFilmWithDirectorsAndRating(final Long id) {
        return filmWithDirectorsMapper.toDTO(filmRepository.findById(id).orElseThrow());
    }

}
