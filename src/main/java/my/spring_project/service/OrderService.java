package my.spring_project.service;

import my.spring_project.dto.FilmDTO;
import my.spring_project.dto.OrderDTO;
import my.spring_project.dto.OrderWithFilmDTO;
import my.spring_project.mapper.FilmMapper;
import my.spring_project.mapper.OrderMapper;
import my.spring_project.mapper.OrderWithFilmMapper;
import my.spring_project.model.Order;
import my.spring_project.repositories.OrderRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class OrderService extends GenericService<Order, OrderDTO> {

    private final FilmMapper filmMapper;
    private final OrderWithFilmMapper orderWithFilmMapper;


    protected OrderService(OrderRepository repository, OrderMapper mapper,
                           FilmMapper filmMapper,
                           OrderWithFilmMapper orderWithFilmMapper) {
        super(repository, mapper);
        this.filmMapper = filmMapper;
        this.orderWithFilmMapper = orderWithFilmMapper;
    }


    public List<FilmDTO> getFilmsOfUser(Long userId) {
        return repository.findAll().stream().
                filter(order -> (order.getUser().getId().equals(userId)))
                .map(order -> filmMapper.toDTO(order.getFilm()))
                .toList();
    }

    public OrderDTO orderFilm(final OrderDTO orderDTO) {
        long rentPeriod = orderDTO.getRentPeriod() != null ? orderDTO.getRentPeriod() : 10L;
        orderDTO.setRentPeriod((int)rentPeriod);
        orderDTO.setRentDate(LocalDate.now());
        orderDTO.setPurchase(true);
        return mapper.toDTO(repository.save(mapper.toEntity(orderDTO)));
    }


    public void returnFilm(Long id) {
        OrderDTO orderDTO = mapper.toDTO(repository.findById(id).orElseThrow());
        orderDTO.setPurchase(false);
        repository.save(mapper.toEntity(orderDTO));
    }

    public Page<OrderWithFilmDTO> getAllOrdersByUserId(Long id, PageRequest pageRequest) {
        Page<Order> ordersPaginated = ((OrderRepository) repository).findOrdersByUserId(id, pageRequest);
        List<OrderWithFilmDTO> result = orderWithFilmMapper.toDTOs(ordersPaginated.getContent());
        return new PageImpl<>(result, pageRequest, ordersPaginated.getTotalElements());
    }

    public Page<OrderWithFilmDTO> getAllActiveOrdersByUserId(Long id, PageRequest pageRequest) {
        Page<Order> ordersPaginated = ((OrderRepository) repository).findOrdersByUserIdAndPurchaseIsTrue(id, pageRequest);
        List<OrderWithFilmDTO> result = orderWithFilmMapper.toDTOs(ordersPaginated.getContent());
        return new PageImpl<>(result, pageRequest, ordersPaginated.getTotalElements());
    }
}
