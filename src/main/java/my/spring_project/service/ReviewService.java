package my.spring_project.service;

import my.spring_project.dto.ReviewDTO;
import my.spring_project.dto.ReviewWithFilmDTO;
import my.spring_project.mapper.ReviewMapper;
import my.spring_project.mapper.ReviewWithFilmMapper;
import my.spring_project.model.Review;
import my.spring_project.repositories.ReviewRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class ReviewService extends GenericService<Review, ReviewDTO> {

    private final ReviewRepository reviewRepository;
    private final ReviewWithFilmMapper reviewWithFilmMapper;

    protected ReviewService(ReviewRepository reviewRepository, ReviewMapper mapper, ReviewWithFilmMapper reviewWithFilmMapper) {
        super(reviewRepository, mapper);
        this.reviewWithFilmMapper = reviewWithFilmMapper;
        this.reviewRepository = reviewRepository;
    }

    public ReviewDTO add(ReviewDTO reviewDTO) {
        reviewDTO.setCreatedWhen(LocalDateTime.now());
        return mapper.toDTO(repository.save(mapper.toEntity(reviewDTO)));
    }

    public List<ReviewWithFilmDTO> getAllReviewsByUserId(Long id) {
        return reviewWithFilmMapper.toDTOs(reviewRepository.findReviewsByUserIdOrderByFilm(id));
    }

    public List<ReviewDTO> getFilmReviewsOrderByCreatedWhen(Long id) {
        return mapper.toDTOs(reviewRepository.findReviewsByFilmIdOrderByCreatedWhen(id));
    }
}
