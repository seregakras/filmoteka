package my.spring_project.service;

import my.spring_project.dto.RoleDTO;
import my.spring_project.mapper.RoleMapper;
import my.spring_project.model.Role;
import my.spring_project.repositories.RoleRepository;
import org.springframework.stereotype.Service;

@Service
public class RoleService extends GenericService<Role, RoleDTO> {

    protected RoleService(RoleRepository roleRepository, RoleMapper roleMapper) {
        super(roleRepository, roleMapper);
    }

}
