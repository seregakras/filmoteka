package my.spring_project.service.userdetail;

import my.spring_project.model.User;
import my.spring_project.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Value("${spring.security.user.name}")
    private String adminUserName;
    @Value("${spring.security.user.password}")
    private String adminPassword;

    public CustomUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        List<GrantedAuthority> authorities = new ArrayList<>();
        if(username.equals(adminUserName)) {
            authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
           return new CustomUserDetails(
                    null,
                    adminUserName,
                    adminPassword,
                    authorities);
        }
        User user = userRepository.findUserByLogin(username);
        authorities.add(new SimpleGrantedAuthority("ROLE_" + user.getRole().getTitle()));
        return new CustomUserDetails(
                user.getId().intValue(),
                user.getLogin(),
                user.getPassword(),
                authorities);
    }
}
